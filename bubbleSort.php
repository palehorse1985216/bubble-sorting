function bubbleSort($list) {
    $length = count($list);
    for ($i=$length - 1; $i>=0; $i--) {
        for ($j=0; $j<$i; $j++) {
            if ($list[$j] > $list[$j+1]) {
                $temp = $list[$j];
                $list[$j] = $list[$j+1];
                $list[$j+1] = $temp;
            }
        }
    }
    return $list;
}